import { RESTDataSource } from "@apollo/datasource-rest";

export default class BooksApi extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = process.env.BOOKS;
  }

  getBooks() {
    return null;
  }
}
