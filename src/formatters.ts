import type { BookType } from "./generated/graphql";
import type { BookType as DataSourceBookType } from "./data-sources/types";

export const formatBooks = (books: DataSourceBookType[]): BookType[] =>
  books.map((book: DataSourceBookType) => ({
    ...book,
    price: {
      value: book.price,
      formatted: `${book.price} €`,
    },
    synopsis: book.synopsis?.[0] || "",
  }));

export const filterBooksBy = (keyword: string, books: DataSourceBookType[]): DataSourceBookType[] =>
  books.filter((book) => {
    if (!book.title && !book.synopsis?.length) return [];

    if (book.title)
      return book.title.toLowerCase().includes(keyword.toLowerCase());

    return (
      book.synopsis?.length &&
      book.synopsis[0].toLowerCase().includes(keyword.toLowerCase())
    );
  });