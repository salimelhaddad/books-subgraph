import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";
import { buildSubgraphSchema } from "@apollo/subgraph";

import { readFileSync } from "fs";
import gql from "graphql-tag";


import resolvers  from "./resolvers";
import BooksApi from "./data-sources/BooksApi";

const typeDefs = gql(
  readFileSync("./src/books.graphql", { encoding: "utf-8" })
);

async function startApolloServer() {
  const server = new ApolloServer({
    // TODO replace any casting by a proper type
    schema: buildSubgraphSchema({ typeDefs, resolvers: resolvers as any }),
  });

  const port = 4000;
  const subgraphName = "reviews";

  try {
    const { url } = await startStandaloneServer(server, {
      context: async () => {
        return {
          dataSources: {
            booksApi: new BooksApi(),
          },
        };
      },
      listen: { port },
    });

    console.log(`🚀 Subgraph ${subgraphName} running at ${url}`);
  } catch (err) {
    console.error(err);
  }
}

startApolloServer();
