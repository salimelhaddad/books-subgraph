import { filterBooksBy, formatBooks } from "./formatters";
import type { Resolvers } from "./generated/graphql";

const resolvers: Resolvers = {
  Query: {
    async books(obj, { keyword }, { dataSources }) {
      try {
        const books = await dataSources.booksApi.getBooks();
        if (keyword) {
          const filtered = filterBooksBy(keyword, books);
          return formatBooks(filtered);
        }
        return formatBooks(books);
      } catch (e) {
        console.log(e, "error");
      }
      return [];
    },
  },
};

export default resolvers;
